var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/pocetna');
var slikeRouter = require('./routes/slike');
const Sequelize = require('sequelize');
const db = require('./db.js');


var index = express();

index.use(logger('dev'));
index.use(express.json());
index.use(express.urlencoded({ extended: false }));
index.use(cookieParser());
index.use(express.static(path.join(__dirname, 'public')));

index.use('/', indexRouter);
index.use('/slike', slikeRouter);

index.get('/',function(req,res){
    res.sendFile(__dirname+'/public/pocetna.html');
  });
  index.get('/pocetna.html',function(req,res){
    res.sendFile(__dirname+'/public/pocetna.html');
  });
  index.get('/rezervacija.html',function(req,res){
    res.sendFile(__dirname+'/rezervacija.html');
  });
  index.get('/sale.html',function(req,res){
    res.sendFile(__dirname+'/sale.html');
  });
 
  index.get('/unos.html',function(req,res){
    res.sendFile(__dirname+'/unos.html');
  });
  index.get('/osoblje.html',function(req,res){
    res.sendFile(__dirname+'/osoblje.html');
  });
  
  index.get('/zauzeca.json',function(req,res){
    res.sendFile(__dirname+'/zauzeca.json');
  });

  index.get('/sale',function(req,res){
    db.Rezervacija.findAll({}).then(function(rez){
        db.Osoblje.findAll({}).then(function(y){
            db.Termin.findAll({}).then(function(z){
                db.Sala.findAll({}).then(function(k){
                    var odgovor = [rez, y, z, k];
                    res.json(odgovor);
                });
            });
        });
    });
  });
 
index.get('/rezervacija', function(req, res){
   
    db.Osoblje.findAll({}).then(osoblje=>{
        if(osoblje){
            let osobljeData = [];
            osoblje.forEach(o => {
                osobljeData.push(o);
            });
            
            res.status(200).json({success:true, data:osobljeData});
         }else {
            res.status(500).json({success:false, msg:'Dobavljanje osoblja nije uspjelo'})
        }
    });
    
})

function inicijalizacija(){
    var osobeListaPromisea=[];
    var saleListaPromisea=[];
    var terminiListaPromisea=[];
    var rezervacijeListaPromisea=[];
    

    return new Promise(function(resolve,reject){
      osobeListaPromisea.push(db.Osoblje.create({id:1,ime:'Neko',prezime:'Nekić',uloga:'profesor'}));
      osobeListaPromisea.push(db.Osoblje.create({id:2,ime:'Drugi',prezime:'Neko',uloga:'asistent'}));
      osobeListaPromisea.push(db.Osoblje.create({id:3,ime:'test',prezime:'test',uloga:'asistent'}));
      

      
      Promise.all(osobeListaPromisea).then(function(osobe){
          var osoba1=osobe.filter(function(o){return o.ime==='Neko'})[0];
          var osoba2=osobe.filter(function(o){return o.ime==='Drugi'})[0];
          var osoba3=osobe.filter(function(o){return o.ime==='test'})[0];

          
          saleListaPromisea.push(db.Sala.create({id:1,naziv:'1-11'}).then(function(s){
            return s.setOsoblje(osoba1).then(function(){return new Promise(function(resolve,reject){resolve(s);});});
          }));
          saleListaPromisea.push(db.Sala.create({id:2,naziv:'1-15'}).then(function(s){
            return s.setOsoblje(osoba1).then(function(){return new Promise(function(resolve,reject){resolve(s);});});
          }));

          Promise.all(saleListaPromisea).then(function(sale){
            var sala1=sale.filter(function(s){return s.naziv==='1-11'})[0];
            var sala2=sale.filter(function(s){return s.naziv==='1-15'})[0];

            terminiListaPromisea.push(db.Termin.create({id:1,redovni:false,dan:null,datum:'01.01.2020',semestar:null,pocetak:'12:00',kraj:'13:00'}));
            terminiListaPromisea.push(db.Termin.create({id:2,redovni:true,dan:0,datum:null,semestar:'zimski',pocetak:'13:00',kraj:'14:00'}));
           
            Promise.all(terminiListaPromisea).then(function(termini){
              var termin1=termini.filter(function(t){return t.pocetak==='12:00'})[0];
              var termin2=termini.filter(function(t){return t.pocetak==='13:00'})[0];

              rezervacijeListaPromisea.push(db.Rezervacija.create({id:1}).then(function(r){
                return Promise.all([r.setOsoblje(osoba1),r.setTermin(termin1),r.setSala(sala1)]).then(function(){
                  return new Promise(function(resolve,reject){resolve(r);});});
              }));
              rezervacijeListaPromisea.push(db.Rezervacija.create({id:2}).then(function(r){
                return Promise.all([r.setOsoblje(osoba3),r.setTermin(termin2),r.setSala(sala1)]).then(function(){
                  return new Promise(function(resolve,reject){resolve(r);});});
              }));

              Promise.all(rezervacijeListaPromisea).then(function(rezervacije){resolve(rezervacije);}).catch(function(err){console.log("Rezervacija greska "+err);});
            }).catch(function(err){console.log("Termin greska "+err);});
          }).catch(function(err){console.log("Sala greska "+err);});
      }).catch(function(err){console.log("Osoblje greska "+err);});
    });
}



db.sequelize.sync({force:true}).then(function(){
    inicijalizacija().then(function(){
        console.log("Gotovo kreiranje tabela i ubacivanje podataka.");
        
    });
});

index.listen(8080);

module.exports = index;

