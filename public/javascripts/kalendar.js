let Kalendar = (function () {
  //ovdje idu privatni atributi
  var periodicnaZauzeca = [];
  var vanrednaZauzeca = [];
  var mjesec = new Date().getMonth();

  
      function periodicno(dan1, semestar1, pocetak1, kraj1, naziv1, predavac1){
        this.dan=dan1;
        this.semestar=semestar1;
        this.pocetak=pocetak1;
        this.kraj=kraj1;
        this.naziv=naziv1;
        this.predavac=predavac1;

      }

      function vanredno(datum1, pocetak1, kraj1, naziv1, predavac1){
        this.datum=datum1;
        this.pocetak=pocetak1;
        this.kraj=kraj1;
        this.naziv=naziv1;
        this.predavac=predavac1;
      }

    /* var objekat1 = new periodicno(6, "zimski", "13:30", "14:30", "MA", "Irfan Prazina" );
  var objekat2 = new periodicno(5, "zimski", "09:30", "11:30", "MA", "Irfan Prazina"  );
  var objekat3 = new periodicno(1, "ljetni", "09:00", "12:00", "EE1", "Vensada Okanović" );
  var objekat4 = new vanredno("22.11.2019", "13:00", "14:30", "VA1", "Irfan Prazina" );
  var objekat5 = new vanredno("15.10.2019", "08:00", "11:00", "VA1", "Vensada Okanović" );
  var nizPeriodicnih = [objekat1, objekat2, objekat3];
  var nizVanrednih = [objekat4, objekat5]; */

  function obojiZauzecaImpl (kalendarRef, mjesec, sala, pocetak, kraj) {

    var kalendar = kalendarRef;
    var dani = kalendar.getElementsByClassName('dan');
    var dani2 = kalendar.getElementsByTagName('td');

    for (var i = 0; i < dani.length; i++) {
      dani[i].children[0].setAttribute('class', 'slobodna');

    }

    //zahtijevanje unosa svih dijelova forme
    if (pocetak == '' || kraj == '') return;
    //pretvaranje vremena u minute
    var vrijeme = pocetak.split(':');
    var vrijeme2 = kraj.split(':');
    var trenutniPocetakMinute = (+vrijeme[0]) * 60 + (+vrijeme[1]);
    var trenutniKrajMinute = (+vrijeme2[0]) * 60 + (+vrijeme2[1]);

    for (var i = 0; i < periodicnaZauzeca.length; i++) {
      var preklapanje = 0;
      if (sala === periodicnaZauzeca[i].naziv) {
        //poredimo vrijeme zauzeca
        var vrijeme3 = periodicnaZauzeca[i].pocetak.split(':');
        var vrijeme4 = periodicnaZauzeca[i].kraj.split(':');
        var pocetakVanrednaMinute = (+vrijeme3[0]) * 60 + (+vrijeme3[1]);
        var krajVanrednaMinute = (+vrijeme4[0]) * 60 + (+vrijeme4[1]);

        if (parseInt(trenutniPocetakMinute) <= parseInt(pocetakVanrednaMinute) & parseInt(krajVanrednaMinute) <= parseInt(trenutniKrajMinute)) preklapanje = 1;
        if (parseInt(trenutniPocetakMinute) <= parseInt(pocetakVanrednaMinute) & parseInt(krajVanrednaMinute) <= parseInt(trenutniKrajMinute) & parseInt(pocetakVanrednaMinute) <= parseInt(trenutniKrajMinute)) preklapanje = 1;
        if (parseInt(trenutniPocetakMinute) >= parseInt(pocetakVanrednaMinute) & parseInt(krajVanrednaMinute) <= parseInt(trenutniKrajMinute) & parseInt(krajVanrednaMinute) >= parseInt(trenutniPocetakMinute)) preklapanje = 1;
        if (parseInt(pocetakVanrednaMinute) <= parseInt(trenutniPocetakMinute) & parseInt(krajVanrednaMinute) >= parseInt(trenutniKrajMinute)) preklapanje = 1;
        //ispitivanje semestra i bojenje periodicnih zauzeca na svakih 7 dana

        if (preklapanje == 1) {
          if (periodicnaZauzeca[i].semestar === 'zimski') {

            if (mjesec >= 9 || mjesec == 0 || mjesec == 11) {

              for (var k = 0; k < dani2.length; k++) {
                if (dani2[k].innerHTML === '') continue;
                if (periodicnaZauzeca[i].dan == k || ((parseInt(periodicnaZauzeca[i].dan)) + 7) == (k)) {

                  for (var j = k; j < dani2.length; j += 7) {

                    dani2[j].children[0].setAttribute('class', 'zauzeta');
                  }
                }
              }
            }
          }

          if (periodicnaZauzeca[i].semestar === 'ljetni') {
            if (mjesec >= 1 & mjesec <= 5) {
              for (var k = 0; k < dani2.length; k++) {
                if (dani2[k].innerHTML === '') continue;

                if (periodicnaZauzeca[i].dan == k || ((parseInt(periodicnaZauzeca[i].dan)) + 7) == (k)) {
                  for (var j = k; j < dani2.length; j += 7) {
                    dani2[j].children[0].setAttribute('class', 'zauzeta');
                  }
                }
              }
            }
          }

        }
      }
    }

    //prolazak kroz niz vanrednih zauzeca istim principom kao i za periodicna
    for (var i = 0; i < vanrednaZauzeca.length; i++) {
      var preklapanje = 0;
      var datumZauzeca = vanrednaZauzeca[i].datum;

      var danZauzeca = datumZauzeca.substring(0, 2);

      var mjesecZauzeca = datumZauzeca.substring(3, 5);
      var vrijeme5 = vanrednaZauzeca[i].pocetak.split(':');
      var vrijeme6 = vanrednaZauzeca[i].kraj.split(':');
      var pocetakVanrednaMinute = (+vrijeme5[0]) * 60 + (+vrijeme5[1]);
      var krajVanrednaMinute = (+vrijeme6[0]) * 60 + (+vrijeme6[1]);

      if (sala === vanrednaZauzeca[i].naziv) {

        if (parseInt(trenutniPocetakMinute) <= parseInt(pocetakVanrednaMinute) & parseInt(krajVanrednaMinute) <= parseInt(trenutniKrajMinute)) preklapanje = 1;
        if (parseInt(trenutniPocetakMinute) <= parseInt(pocetakVanrednaMinute) & parseInt(krajVanrednaMinute) <= parseInt(trenutniKrajMinute) & parseInt(pocetakVanrednaMinute) <= parseInt(trenutniKrajMinute)) preklapanje = 1;
        if (parseInt(trenutniPocetakMinute) >= parseInt(pocetakVanrednaMinute) & parseInt(krajVanrednaMinute) <= parseInt(trenutniKrajMinute) & parseInt(krajVanrednaMinute) >= parseInt(trenutniPocetakMinute)) preklapanje = 1;
        if (parseInt(pocetakVanrednaMinute) <= parseInt(trenutniPocetakMinute) & parseInt(krajVanrednaMinute) >= parseInt(trenutniKrajMinute)) preklapanje = 1;

        if (preklapanje == 1) {
          if (mjesec == (parseInt(mjesecZauzeca) - 1)) {
            for (var j = 0; j < dani.length; j++) {
              if (j == (parseInt(danZauzeca) - 1)) dani[j].children[0].setAttribute('class', 'zauzeta');
            }
          }
        }
      }

    }

  }

  function ucitajPodatkeImpl (periodicna, vanredna) {
    //implementacija ide ovdje
    //prebacuje vrijednosti iz parametara u varijable
    periodicnaZauzeca = periodicna;
    vanrednaZauzeca = vanredna;
  }

  function prethodni () {
    if (mjesec == 1) {
      document.getElementById('prethodni').disabled = true;
    }

    mjesec--;
    if (mjesec == 10) {
      document.getElementById('sljedeci').disabled = false;
    }

    Kalendar.iscrtajKalendar(document.getElementById('kalendar'), mjesec);
    var Sala1 = document.getElementsByName('sale')[0].value;
    var Pocetak1 = document.getElementsByName('pocetak')[0].value;
    var Kraj1 = document.getElementsByName('kraj')[0].value;
    var kalendarRef = document.getElementById('kalendar');
    Kalendar.obojiZauzeca(kalendarRef, mjesec, Sala1, Pocetak1, Kraj1);

  }

  function sljedeci () {
    if (mjesec == 10) {
      document.getElementById('sljedeci').disabled = true;
    }

    mjesec++;
    if (mjesec == 1) {
      document.getElementById('prethodni').disabled = false;
    }

    Kalendar.iscrtajKalendar(document.getElementById('kalendar'), mjesec);
    var Sala1 = document.getElementsByName('sale')[0].value;
    var Pocetak1 = document.getElementsByName('pocetak')[0].value;
    var Kraj1 = document.getElementsByName('kraj')[0].value;
    var kalendarRef = document.getElementById('kalendar');
    Kalendar.obojiZauzeca(kalendarRef, mjesec, Sala1, Pocetak1, Kraj1);
  }

  function vratiBrojDana (mjesec) {
    if (mjesec == 0) return 31;
    else if (mjesec == 1) return 28;
    else if (mjesec == 2) return 31;
    else if (mjesec == 3) return 30;
    else if (mjesec == 4) return 31;
    else if (mjesec == 5) return 30;
    else if (mjesec == 6) return 31;
    else if (mjesec == 7) return 31;
    else if (mjesec == 8) return 30;
    else if (mjesec == 9) return 31;
    else if (mjesec == 10) return 30;
    else return 31;
  }

  function iscrtajKalendarImpl (kalendarRef, mjesec) {
    //implementacija ide ovdje
    if (mjesec == 11) {
      document.getElementById('sljedeci').disabled = true;
    }

    if (mjesec == 0) {
      document.getElementById('prethodni').disabled = true;
    }
    document.getElementById('kalendar').innerHTML = '';
    kalendarRef = document.getElementById('kalendar');

    //odredjivanje prvog dana u mjesecu
    var datum = new Date(2019, mjesec, 1);
    var praznaMjesta = 6;
    var prviDan = datum.getDay();
    if (prviDan == 0) {prviDan = 7;}

    //ispis naziva mjeseca
    var nazivMjesecaTR = document.createElement('tr');

    var nazivMjesecaTH = document.createElement('th');
    nazivMjesecaTH.setAttribute('class', 'novembar');
    nazivMjesecaTH.setAttribute('colspan', '7');
    if (mjesec == '0') {nazivMjesecaTH.innerText = 'Januar';} else if (mjesec == '1') {nazivMjesecaTH.innerText = 'Februar';} else if (mjesec == '2') {nazivMjesecaTH.innerText = 'Mart';} else if (mjesec == '3') {nazivMjesecaTH.innerText = 'April';} else if (mjesec == '4') {nazivMjesecaTH.innerText = 'Maj';} else if (mjesec == '5') {nazivMjesecaTH.innerText = 'Juni';} else if (mjesec == '6') {nazivMjesecaTH.innerText = 'Juli';} else if (mjesec == '7') {nazivMjesecaTH.innerText = 'August';} else if (mjesec == '8') {nazivMjesecaTH.innerText = 'Septembar';} else if (mjesec == '9') {nazivMjesecaTH.innerText = 'Oktobar';} else if (mjesec == '10') {nazivMjesecaTH.innerText = 'Novembar';} else nazivMjesecaTH.innerText = 'Decembar';

    nazivMjesecaTR.appendChild(nazivMjesecaTH);

    document.getElementById('kalendar').appendChild(nazivMjesecaTR);

    //dani u sedmici
    var sedmicaTR = document.createElement('tr');
    sedmicaTR.setAttribute('class', 'sedmica');
    for (var i = 0; i < 7; i++) {
      var TH = document.createElement('th');
      TH.setAttribute('class', 'naziv');
      if (i == 0) TH.innerText = 'PON';
      else if (i == 1) TH.innerText = 'UTO';
      else if (i == 2) TH.innerText = 'SRI';
      else if (i == 3) TH.innerText = 'ČET';
      else if (i == 4) TH.innerText = 'PET';
      else if (i == 5) TH.innerText = 'SUB';
      else TH.innerText = 'NED';
      sedmicaTR.appendChild(TH);
    }

    document.getElementById('kalendar').appendChild(sedmicaTR);

    //prva sedmica
    var prvaSedmicaTR = document.createElement('tr');
    prvaSedmicaTR.setAttribute('class', 'prvaSedmica');
    for (var i = 1; i < prviDan; i++) {
      var TD = document.createElement('td');
      TD.setAttribute('class', 'prazno');
      TD.innerText = '';
      prvaSedmicaTR.appendChild(TD);
    }
    var brojacDana = 1;
    for (i = prviDan; i < 8; i++) {
      var TD = document.createElement('td');
      TD.setAttribute('class', 'dan');
      TD.innerText = (brojacDana).toString();
      var DIV = document.createElement('div');
      DIV.setAttribute('class', 'slobodna');
      DIV.innerHTML = '&nbsp';
      TD.appendChild(DIV);
      prvaSedmicaTR.appendChild(TD);
      brojacDana++;

    }
    document.getElementById('kalendar').appendChild(prvaSedmicaTR);

    //kreiranje svih dana u mjesecu do posljednjeg
    while (brojacDana < (vratiBrojDana(mjesec) + 1)) {
      var drugaSedmicaTR = document.createElement('tr');
      drugaSedmicaTR.setAttribute('class', 'obrisi');
      for (i = 0; i < 7; i++) {
        var TD = document.createElement('td');
        TD.setAttribute('class', 'dan');
        TD.innerText = (brojacDana).toString();
        var DIV = document.createElement('div');
        DIV.setAttribute('class', 'slobodna');
        DIV.innerHTML = '&nbsp';
        TD.appendChild(DIV);
        drugaSedmicaTR.appendChild(TD);
        brojacDana++;
        if (brojacDana == (vratiBrojDana(mjesec)) + 1) break;
      }
      document.getElementById('kalendar').appendChild(drugaSedmicaTR);

    }

  }

   window.onload=function(){

     var mjesec = new Date().getMonth();
     Kalendar.iscrtajKalendar(document.getElementById("kalendar") , mjesec);
   

   } 

  function funkcija3 () {
    var Sala1 = document.getElementsByName('sale')[0].value;
    var Pocetak1 = document.getElementsByName('pocetak')[0].value;
    var Kraj1 = document.getElementsByName('kraj')[0].value;
    var kalendarRef = document.getElementById('kalendar');
    Kalendar.obojiZauzeca(kalendarRef, mjesec, Sala1, Pocetak1, Kraj1);
  }

  return {

    obojiZauzeca: obojiZauzecaImpl,
    iscrtajKalendar: iscrtajKalendarImpl,
    ucitajPodatke: ucitajPodatkeImpl,
    prethodni: prethodni,
    sljedeci: sljedeci,
    vratiBrojDana: vratiBrojDana,
    funkcija3: funkcija3
  };
}());


