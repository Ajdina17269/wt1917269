/* implementacije funkcija, ajax */
let Pozivi = (function () {

  function ucitajZauzeca () {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
      if (ajax.readyState == 4 && ajax.status == 200) {
        var zauzecaPodaci = JSON.parse(ajax.responseText);
        
        var periodicnaZauzeca1 = zauzecaPodaci.periodicno;
        var vanrednaZauzeca1 = zauzecaPodaci.vanredno;
        Kalendar.ucitajPodatke(periodicnaZauzeca1, vanrednaZauzeca1);
      }
      if (ajax.readyState == 4 && ajax.status == 404) {
        console.log('Greska!');
      }
    };
    ajax.open('GET', '/zauzeca.json', true);
    ajax.send();
  }

  function osobljeSaleImpl (x){
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
      if (ajax.readyState == 4 && ajax.status == 200) {
        var dohvaceneRezervacije = JSON.parse(ajax.responseText);
    //  console.log(dohvaceneRezervacije);
      var rezervacijeNiz = dohvaceneRezervacije[0];
      var osobeNiz = dohvaceneRezervacije[1];
      var terminiNiz = dohvaceneRezervacije[2];
      var saleNiz = dohvaceneRezervacije[3];
      var osobaRezervisala = [];
      let now = new Date();
      let trenutniDatum = now.getDate();
      let trenutniMjesec = now.getMonth() + 1;
      let trenutniDan = now.getDay();
      if(trenutniDan==0) trenutniDan=6;
      else trenutniDan--;
      let trenutniSati = now.getHours();
      let trenutneMinute = now.getMinutes();
      let trenutnoVrijeme = trenutniSati * 60 + trenutneMinute;
      let naziviSala = [];
      let naziviOsoba = [];
      let uKancelariji = true;

      var tbl = document.getElementById('prva');
      if(tbl) tbl.parentNode.removeChild(tbl);
      //tabela.setAttribute("id", "prva");

     // let periodicno = true;
    

      for( var i=0; i<osobeNiz.length; i++){
        
        uKancelariji = true;
        let ime = osobeNiz[i].ime + ' ' + osobeNiz[i].prezime;
        naziviOsoba.push(ime);

        //console.log(osobeNiz[0].id);
        for(var j=0; j<rezervacijeNiz.length; j++){
          if(rezervacijeNiz[j].osoba == osobeNiz[i].id) 
          osobaRezervisala.push(rezervacijeNiz[j]);
        }
        for(var k=0; k<osobaRezervisala.length; k++){
            for(var s=0; s<terminiNiz.length; s++){
              if(osobaRezervisala[k].termin==terminiNiz[s].id){
                if(terminiNiz[s].dan == null){
                  
                   // console.log(terminiNiz[s].datum.substr(3,2));
                  var mjesec = parseInt(terminiNiz[s].datum.substr(3,2));
                  if(mjesec<10) mjesec = parseInt(terminiNiz[s].datum.substr(4,1));
                 
                  if(mjesec == trenutniMjesec){
                    //uslo
                  var dan = parseInt(terminiNiz[s].datum.substr(0,2));
                  //console.log(dan);
                 // console.log(terminiNiz[s]);
                    if(dan<10) dan = parseInt(terminiNiz[s].datum.substr(1,1));
                    //console.log(trenutniDatum);
                    if(dan == trenutniDatum){
                      let pocetakSati =parseInt(terminiNiz[s].pocetak.substr(0,2));
                      let pocetakMinute = parseInt(terminiNiz[s].pocetak.substr(3,2));
                      let vrijemePocetka = pocetakSati * 60 + pocetakMinute;

                      let krajSati = parseInt(terminiNiz[s].kraj.substr(0,2));
                      let krajMinute = parseInt(terminiNiz[s].kraj.substr(3,2));
                      let vrijemeKraja = krajSati * 60 + krajMinute;

                      //ispituje da li je trenutno vrijeme u vremenu termina u bazi
                      if(trenutnoVrijeme >= vrijemePocetka && trenutnoVrijeme <= vrijemeKraja){
                        
                          //prolazimo kroz niz sala
                          for(var v=0; v<saleNiz.length; v++){
                            if(saleNiz[v].zaduzenaOsoba == osobeNiz[i].id) {
                              naziviSala.push(saleNiz[v].naziv);
                              uKancelariji = false;
                            }
                          }
                      }

                      

                    }
                  }

                }
                else if(terminiNiz[s].dan != null){
                  uKancelariji=true;
                  if(trenutniDan == terminiNiz[s].dan){

                    
                      let pocetakSati =parseInt(terminiNiz[s].pocetak.substr(0,2));
                      let pocetakMinute = parseInt(terminiNiz[s].pocetak.substr(3,2));
                      let vrijemePocetka = pocetakSati * 60 + pocetakMinute;

                      let krajSati = parseInt(terminiNiz[s].kraj.substr(0,2));
                      let krajMinute = parseInt(terminiNiz[s].kraj.substr(3,2));
                      let vrijemeKraja = krajSati * 60 + krajMinute;

                      //ispituje da li je trenutno vrijeme u vremenu termina u bazi
                      if(trenutnoVrijeme >= vrijemePocetka && trenutnoVrijeme <= vrijemeKraja){
                        
                          //prolazimo kroz niz sala
                          for(var v=0; v<saleNiz.length; v++){
                            if(saleNiz[v].zaduzenaOsoba == osobeNiz[i].id) {
                              naziviSala.push(saleNiz[v].naziv);
                              uKancelariji = false;
                            }
                          }
                  }

                }
              }

            }
        }
      }
    
        if(uKancelariji) {
          naziviSala.push("U kancelariji");
        }
      }
      
      
      //console.log(naziviOsoba);
      //console.log(naziviSala);
        
        var tabela = document.createElement("table");
        tabela.setAttribute("id", "prva");
        


        for(var i=0; i<naziviOsoba.length; i++){
          var red = tabela.insertRow(i);
          var kolona = red.insertCell(0);
          kolona.innerHTML = naziviOsoba[i] ;
          var kolona2 = red.insertCell(1);
          kolona2.innerHTML = naziviSala[i];
        }

        x.appendChild(tabela);


      }
      if (ajax.readyState == 4 && ajax.status == 404) {
        console.log('Greska!');
      }
    };
    ajax.open('GET', '/sale', true);
    ajax.send();
  }
/**
   *
   * @param type - GET, POST, FETCH, DELETE
   * @param uri - full endpoint
   * @param headers - object
   * @param data - body or payload
   * @returns {Promise<unknown>} - Resolves in callback of APIs
   */
  async function serviceRequest (type, uri, headers, data, raw) {
    const settings = {
      url: uri,
      type: type,
      headers: headers
    };
    return new Promise(function (resolve, reject) {
    
      let http = new XMLHttpRequest();
      http.open(settings.type, settings.url, true);

      
      if (!!headers) {
        Object.keys(headers).forEach((header) => {
          http.setRequestHeader(header.name, header.value);
        });
      }
      http.send(!!data ? data : null);
      http.onreadystatechange = function () {
        if (http.readyState === 4) {
          if (http.status === 200) {
            resolve(raw !== undefined ? http.response : JSON.parse(http.response));
          } else {
            reject(`Error:: ${http.status} @ ${http.responseURL}`);
          }
        }
      };
    });
  }

  const provider = "/slike";
  const headers = [
    {
      name: 'Content-Type',
      value: 'application/json'
    }
  ];



  async function slike (page, records_per_page) {

    const provider =  "/slike/" + page + '/' + records_per_page;

    const headers = [
      {
        name: 'Content-Type',
        value: 'application/json'
      }
    ];
    let image;

    let slike;


    try {
      slike = await serviceRequest('GET', provider, headers);

      return slike;
    } catch (error) {
      console.error(error);
    }
  }

  var objJson = []
  var current_page = 1;
  var records_per_page = 3;
  var total = 0;
  var page = 1;
  function prevPage()
  {
    if (current_page > 1) {
      current_page--;
      changePageforPrevious(current_page, records_per_page);
    }
  }

  function nextPage()
  {
    if (current_page < numPages()) {
      current_page++;
      changePage(current_page, records_per_page);
    }
  }

  function changePage(page, records_per_page)
  {

    var listing_table = document.getElementById("listingTable");
    var page_span = document.getElementById("page");


    slike(page, records_per_page).then(function (response) {
      total = response.total;

     
      if (page <= 1) page = 1;
      if (page > numPages()) page = numPages();

      listing_table.innerHTML = "";

      objJson = response.slike;

      for (var i = (page-1) * records_per_page; i < (page * records_per_page); i++) {
        listing_table.innerHTML += `<img src="images${objJson[i]}" alt="Slika">`;
      }
      page_span.innerHTML = page;

      buttons();
    })

  }

  function changePageforPrevious(page, records_per_page)
  {
    var listing_table = document.getElementById("listingTable");
    var page_span = document.getElementById("page");


    
    if (page <= 1) page = 1;
    if (page > numPages()) page = numPages();

    listing_table.innerHTML = "";

    for (var i = (page-1) * records_per_page; i < (page * records_per_page); i++) {
      if (objJson[i]) {
        listing_table.innerHTML += `<img src="images${objJson[i]}" alt="Slika">`;
      }
    }
    page_span.innerHTML = page;

    buttons()

  }

  function numPages()
  {
    return Math.ceil(totalImages() / records_per_page);
  }

  function buttons() {
    var naprijed = document.getElementById("dugmeSljedeci");
    var nazad = document.getElementById("dugmePrethodni");
    if (current_page == 1) {
      nazad.disabled = true;
    } else {
      nazad.disabled = false;
    }

    if (current_page == numPages()) {
      naprijed.disabled= true;
    } else {
      naprijed.disabled = false;
    }
  }
  

  function totalImages() {
    return total;
  }



  return {
    ucitajPodatke1: ucitajZauzeca,
    osobljeSale: osobljeSaleImpl,
    changePage: changePage,
    nextPage: nextPage,
    prevPage: prevPage,
    buttons: buttons
  };
}());

