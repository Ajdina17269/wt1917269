const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19","root","root",{host:"127.0.0.1",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;
db.sequelize = sequelize;


db.Osoblje = sequelize.import(__dirname+'/Osoblje.js');
db.Rezervacija = sequelize.import(__dirname+'/Rezervacija.js');
db.Termin = sequelize.import(__dirname+'/Termin.js');
db.Sala = sequelize.import(__dirname+'/Sala.js');


db.Osoblje.hasMany(db.Rezervacija,{foreignKey:'osoba'});
db.Rezervacija.belongsTo(db.Osoblje,{foreignKey:'osoba'});


db.Termin.hasOne(db.Rezervacija,{foreignKey:'termin'});
db.Rezervacija.belongsTo(db.Termin,{foreignKey:'termin'});


db.Sala.hasMany(db.Rezervacija,{foreignKey:'sala'});
db.Rezervacija.belongsTo(db.Sala,{foreignKey:'sala'});


db.Osoblje.hasOne(db.Sala,{foreignKey:'zaduzenaOsoba'});
db.Sala.belongsTo(db.Osoblje,{foreignKey:'zaduzenaOsoba'});


module.exports=db;
