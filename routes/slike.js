var express = require('express');
var router = express.Router();
var fs = require('fs');

function getFiles (dir, slike) {
  slike = slike || [];
  var files = fs.readdirSync(dir);
  for (var i in files) {
    var name = dir + '/' + files[i];

    if (fs.statSync(name).isDirectory()) {
      getFiles(name, slike);
    } else {
      name = name.substr(name.lastIndexOf('/'));
      slike.push(name);
    }
  }
  return slike;
}

router.get('/:page/:record', function (req, res, next) {
  const params = req.params;
  res.json({
    'slike': getFiles(__dirname + '/../public/images').slice((params.page-1) * params.records, params.page * params.record),
    'total': getFiles(__dirname + '/../public/images').length
  });
});

module.exports = router;
