var express = require('express');
var router = express.Router();
var path = require('path');

/* dobavljanje pocetne stranice */
router.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname + '/../public/pocetna.html'));
});


module.exports = router;
